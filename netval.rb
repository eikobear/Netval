require "oga"
require "open-uri"
require "pry-byebug"

class Netval
  extend Forwardable

  def_delegators :value, :+, :-, :/, :*, :to_str

  attr_reader :name

  def initialize name, data
    @name = name
    @url = data["url"]
    @mvp = data["mvp"]
    @regex = Regexp.new data["regex"]
  end

  def scrape
    oga_doc = Oga.parse_html open(@url)
    raw = oga_doc.css(@mvp).first.text
    value = @regex.match(raw)[1]
  end

  def value
    @value ||= scrape
  end

  def refresh
    @value = scrape
  end

  def method_missing name, *args, &block
    value.public_send name, *args, &block
  end


end
