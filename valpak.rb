require "yaml"

require_relative "netval"

class Valpak

  VALPAK_DIR = "./ValPaks"
  @@vps = {}

  def self.load pakname
    data = File.open(File.join(VALPAK_DIR, "#{pakname}.yaml"), "r") { |f| YAML.safe_load f }
    vp = Valpak.new pakname
    @@vps[pakname] = vp
    data.keys.each do |key|
      vp.add Netval.new(key, data[key])
    end
    vp
  end

  def initialize name
    @name = name
    @netvals = {}
  end

  def add netval
    @netvals[netval.name] = netval
    define_singleton_method :"#{netval.name}" do
      @netvals[netval.name]
    end
  end

end


