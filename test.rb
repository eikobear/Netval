require "oga"
require "open-uri"
require "launchy"
require "fileutils"
require "yaml"

require "pry"

VALPAK_DIR = "./ValPaks"

def find_path children, value

  children.each do |child|
    if child.text.include? value
      return [child] + find_path(child.children, value)
    end
  end
  return []

end

def find_mvp oga_doc, value
  path = find_path(oga_doc.children, value)

  return nil if path.empty?

  path.pop

  crumbs = path.map { |item| (item.attr(:id)) ? "##{item.attr(:id)&.value}" : ".#{item.attr(:class)&.value}" }.select { |item| item != "." }

  reliable = []
  crumbs.each do |crumb|
    reliable << crumb if oga_doc.css(crumb).count == 1 && oga_doc.css(crumb).first.text.include?(value)
  end

  keystone = reliable.pop
  length = oga_doc.css(keystone).first.text.length

  i = crumbs.index(keystone)
  remainders = crumbs[(i+1)..-1]

  polish = []
  remainders.each do |remainder|
    result = oga_doc.css("#{keystone} #{remainder}")
    if result.count == 1 && result.first.text.length < length
      polish << result.first
    end
  end


  polish.map! { |item| (item.attr(:id)) ? "##{item.attr(:id)&.value}" : ".#{item.attr(:class)&.value}" }.select { |item| item != "." }

  mvp = [keystone, polish.last]
  mvp.join(" ").strip
end

def save_netval packname, valname, data
  Dir.mkdir(VALPAK_DIR) unless Dir.exist?(VALPAK_DIR)
  file_path = File.join(VALPAK_DIR, "#{packname}.yaml")
  if File.exist? file_path
    f.prints " -#{packname} exists. loading... "
    yaml = YAML.safe_load File.open { |f| f.read }
  end
  yaml ||= {}
  data["regex"] = data["regex"].to_s
  yaml[valname] = data
  binding.pry
  File.open(file_path, "w") { |f| f.puts YAML.dump(yaml) }
end

print "\nenter a url: "
url = gets.chomp

file = open(url)

print "\n -loading browser... "

browser = Launchy.open file.path


puts "loaded."

print "\nenter the value you would like to capture: "

value = gets.chomp
value = value.gsub("\\n", "\n").gsub("\\\n", "\\n")


print "\n\n -parsing html... "

html = Oga.parse_html file.read


puts "parsed."

print " -finding mvp for #{value.dump}... "

mvp = find_mvp html, value

if mvp.nil?
  puts "failed!"
  puts "\nERROR: could not find a path to your value. double-check your input, or consider filing a bug report."
  exit
end


puts "found."

puts "\nmvp: #{mvp}"

print "\n -testing mvp... "

mvp_test = html.css(mvp)


if mvp_test.count > 1
  puts "failed!"
  puts "\nERROR: mvp found multiple values! please file a bug report."
  exit
end

if !mvp_test.first.text.include? value
  puts "failed!"
  puts "\nERROR: mvp generated text which did not include the original value! please file a bug report."
  exit
end

puts "tested."

test_value = mvp_test.first.text

if value == test_value
  puts " -#{test_value.dump} matches #{value.dump} perfectly!"
  print "generating regex..."
  regex = /(.*)/m
  puts "generated."
  puts "\nregex: #{regex.inspect}"
else
  puts " -#{test_value.dump} is not a perfect match for #{value.dump}"
  print " -generating regex..."
  index = test_value.index value
  length = value.length
  regex = /.{#{index}}(.{#{length}}).*/m
  puts "generated."
  puts "\nregex: #{regex.inspect}"
end

print "\noverride with custom regex? (y/N):"
override = gets.chomp

if override.downcase == "y" || override.downcase == "yes" || override.downcase == "yeah"
  print "\nenter regex (omitting // delimiters): "
  regex = /#{gets.chomp}/m
end
  
print "\n -testing regex #{regex.inspect}..."

v = regex.match(test_value)[1]

if v != value
  puts "failed!"
  puts "\nERROR: regex #{regex.inspect} on #{test_value.dump} produces #{v.dump} which does not match #{value.dump}! please file a bug report."
  exit
end

puts "tested."
puts " -regex #{regex.inspect} on #{test_value.dump} produces #{v.dump} which matches #{v.dump}"

print "\ngenerate netval? (Y/n):"
cmd = gets.chomp

if ["y", "yes", "yeah", "yee", "yup", ""].include? cmd.downcase
  print "\nenter netval name (format: PackName.val_name): "
  name = gets.chomp
  packName, valName = *name.split(".")
  print "\n -saving #{valName} in #{packName}... "
  save_netval packName, valName, "url" => url, "mvp" => mvp, "regex" => regex
  puts "saved."
end 

